var PageView = require('./base');


module.exports = PageView.extend({
    pageTitle: 'more info',
    dustTemplateName: 'pages.info'
});
