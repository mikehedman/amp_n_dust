var PageView = require('./base');


module.exports = PageView.extend({
    pageTitle: 'home',
    dustTemplateName: 'pages.home'
});
