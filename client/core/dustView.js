/**
 * Overrides base Ampersand View class to make it work with Dust templates
 * Implementers of this class must provide a dustTemplateName instead of using the template property
 */
var View = require('ampersand-view');
var _ = require('underscore');

var DustView = View.extend({
  template: '<H2>Error, no template found for dustTemplateName: ' + this.dustTemplateName + '</H2>',
  dustTemplateName: '',
  render: function() {
    var self = this;
    if (_.isEmpty(self.dustTemplateName)) {
      debugger;
      self.renderWithTemplate(self, '<h2>Every view must assign a value to the "dustTemplateName" property</h2>');
    } else {
      dust.render(this.dustTemplateName, self, function(err, out){
        self.renderWithTemplate(self, out);
      });
    }
  }
});

module.exports = DustView;