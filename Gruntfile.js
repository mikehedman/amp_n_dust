var path = require("path");

module.exports = function(grunt) {

  var templateCwd = 'templates-dust/';
  var templateFiles = '**/*.dust';
  var templateDestDir = 'public/templates-js/';

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    dustjs: {
      compile: {
        files: [
          {
            cwd: templateCwd,
            expand: true,
            src: templateFiles,
            dest: templateDestDir,
            ext: ".js"
          }
        ],
        options: {
          fullname: function(filepath) {
            var key = path.relative(templateCwd, path.dirname(filepath)).split(path.sep) // folder names
                .concat([path.basename(filepath, path.extname(filepath))]) // template name
                .join(".");

            if (key.charAt(0) == ".")
              return key.substr(1, key.length - 1);
            return key;
          }
        }
      }
    },
    delta: {
      cwd: templateCwd,
      files: [templateFiles],
      tasks: ['dustjs', 'concat']
    },

    concat: {
      dist: {
        src: [templateDestDir + '/**/*.js'],
        dest: 'client/allDustTemplates.js'
      },
      options: {
        banner: "exports.registerAll = function() {",
        footer: "};"
      }
    }
  });

  grunt.loadNpmTasks("grunt-dustjs");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks('grunt-contrib-concat');
//  grunt.loadTasks('tasks');

  /**
   * In order to make it safe to just compile or copy *only* what was changed,
   * we need to ensure we are starting from a clean, fresh build. So we rename
   * the `watch` task to `delta` (that's why the configuration var above is
   * `delta`) and then add a new task called `watch` that does a clean build
   * before watching for changes.
   */
  grunt.renameTask( 'watch', 'delta' );
  grunt.registerTask( 'watch', ['delta'] );

  grunt.registerTask('default', ['dustjs', 'concat']);
};